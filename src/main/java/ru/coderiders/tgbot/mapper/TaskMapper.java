package ru.coderiders.tgbot.mapper;

import org.springframework.stereotype.Component;
import ru.coderiders.tgbot.dto.TaskDto;
import ru.coderiders.tgbot.dto.TaskRqDto;

@Component
public class TaskMapper {
    public TaskRqDto toTaskRequest(TaskDto taskDto){
        return TaskRqDto.builder()
                .animalId(taskDto.getAnimalId())
                .expiresDateTime(taskDto.getExpiresDateTime())
                .completed(taskDto.isCompleted())
                .note(taskDto.getNote())
                .repeatType(taskDto.getRepeatType())
                .type(taskDto.getType())
                .build();
    }
}
