package ru.coderiders.tgbot.handler;

import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import ru.coderiders.tgbot.bot.InlineKeyboard;
import ru.coderiders.tgbot.constants.ButtonNames;
import ru.coderiders.tgbot.constants.MessageTemplates;
import ru.coderiders.tgbot.dto.TaskDto;
import ru.coderiders.tgbot.dto.UserDto;
import ru.coderiders.tgbot.mapper.TaskMapper;
import ru.coderiders.tgbot.utils.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

@Component
@AllArgsConstructor
public class CallbackQueryHandler {
    private MessageHandler messageHandler;
    private TaskMapper taskMapper;
    InlineKeyboard inlineKeyboard;

    public BotApiMethod<?> processCallbackQuery(CallbackQuery callbackQuery) throws IOException {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(callbackQuery.getMessage().getChatId().toString());
        if (Objects.equals(callbackQuery.getData(), ButtonNames.AUTHORIZE.getName())) {
            return new SendMessage(callbackQuery.getMessage().getChatId().toString(),
                    MessageTemplates.PLEASE_ENTER_CREDENTIALS.getMessage());
        } else if (callbackQuery.getData().contains("id")) {
            return sendTaskById(callbackQuery, sendMessage);
        } else if (callbackQuery.getData().contains("doneId")) {
            return sendTaskUpdated(callbackQuery, sendMessage);
        } else if (callbackQuery.getData().contains("next")){
            return sendNextTaskPage(callbackQuery, sendMessage);
        }
        return null;
    }

    private BotApiMethod<?> sendNextTaskPage(CallbackQuery callbackQuery, SendMessage sendMessage) throws IOException {
        return messageHandler.getTasks(sendMessage, callbackQuery.getData().split(" ")[1]);
    }

    private SendMessage sendTaskUpdated(CallbackQuery callbackQuery, SendMessage sendMessage) throws IOException {
        UserDto userDto = messageHandler.getUserDto();
        TaskDto taskDto = getTaskById(callbackQuery);
        taskDto.setCompleted(true);
        URL url = new URL("http://localhost:8080/api/tasks/" + callbackQuery.getData().split(" ")[1]);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("PUT");
        con.setRequestProperty("Content-Type", "application/json; utf-8");
        con.setRequestProperty("Accept", "application/json");
        con.setRequestProperty("Authorization", "Bearer " + userDto.getToken());
        con.setDoOutput(true);
        String json = new Gson().toJson(taskMapper.toTaskRequest(taskDto));
        try (OutputStream os = con.getOutputStream()) {
            byte[] input = json.getBytes(StandardCharsets.UTF_8);
            os.write(input, 0, input.length);
        }
        con.connect();
        con.getResponseCode();
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8))) {
            StringBuilder response = new StringBuilder();
            String responseLine;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }
            TaskDto taskDto1 = new Gson().fromJson(response.toString(), TaskDto.class);
            if (taskDto1.isCompleted()) {
                sendMessage.setText(MessageTemplates.TASK_COMPLETED.getMessage());
                return sendMessage;
            }
        }
        return null;
    }

    private SendMessage sendTaskById(CallbackQuery callbackQuery, SendMessage sendMessage) throws IOException {
        TaskDto taskDto = getTaskById(callbackQuery);
        String message = StringUtils.formatTask(taskDto);
        sendMessage.setText(message);
        sendMessage.setReplyMarkup(inlineKeyboard.getDoneKeyboard(taskDto.getId().toString()));
        return sendMessage;
    }

    private TaskDto getTaskById(CallbackQuery callbackQuery) throws IOException {
        UserDto userDto = messageHandler.getUserDto();
        URL url = new URL("http://localhost:8080/api/tasks/" + callbackQuery.getData().split(" ")[1]);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("Content-Type", "application/json; utf-8");
        con.setRequestProperty("Authorization", "Bearer " + userDto.getToken());
        con.setDoOutput(true);
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8))) {
            StringBuilder response = new StringBuilder();
            String responseLine;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }
            return new Gson().fromJson(response.toString(), TaskDto.class);
        }
    }

}
