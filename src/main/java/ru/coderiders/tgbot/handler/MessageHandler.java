package ru.coderiders.tgbot.handler;

import com.google.gson.Gson;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import ru.coderiders.tgbot.bot.InlineKeyboard;
import ru.coderiders.tgbot.bot.Keyboard;
import ru.coderiders.tgbot.constants.ButtonNames;
import ru.coderiders.tgbot.constants.Commands;
import ru.coderiders.tgbot.constants.MessageTemplates;
import ru.coderiders.tgbot.dto.PageDto;
import ru.coderiders.tgbot.dto.UserDto;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.Optional;

@Component
public class MessageHandler {
    private final Keyboard keyboard;
    private final InlineKeyboard inlineKeyboard;

    UserDto userDto;
    Long userId;

    public MessageHandler(Keyboard keyboard, InlineKeyboard inlineKeyboard) {
        this.keyboard = keyboard;
        this.inlineKeyboard = inlineKeyboard;
    }

    public BotApiMethod<Message> answerMessage(Message message) throws IOException {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(message.getChatId().toString());
        if (message.getText().contains(Commands.START.getName())) {
            userId = Optional.of(Long.valueOf(message.getText().split(" ")[1])).orElse(2L);
            authorize(sendMessage);
        } else if (Objects.equals(ButtonNames.GET_TASKS.getName(), message.getText())) {
            return getTasks(sendMessage, String.valueOf(0));
        }
        return sendMessage;
    }

    private void authorize(SendMessage sendMessage) throws IOException {
        URL url = new URL("http://localhost:8080/api/auth/login");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json; utf-8");
        con.setDoOutput(true);
        String json = "{\n" +
                "  \"login\": \"tgadmin\",\n" +
                "  \"password\": \"tgadmin\"\n" +
                "}";
        try (OutputStream os = con.getOutputStream()) {
            byte[] input = json.getBytes(StandardCharsets.UTF_8);
            os.write(input, 0, input.length);
        }

        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8))) {
            StringBuilder response = new StringBuilder();
            String responseLine;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }
            userDto = new Gson().fromJson(response.toString(), UserDto.class);
            userDto.setId(userId);
            sendMessage.setText(MessageTemplates.GREETING.getMessage());
            sendMessage.setReplyMarkup(keyboard.getMainMenuKeyboard());
        }
    }

    public SendMessage getTasks(SendMessage sendMessage, String pageNumber) throws IOException {
        URL url = new URL("http://localhost:8080/api/tasks?page=" + pageNumber
                + "&size=8&userId=" + userDto.getId() + "&sort=id");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("Content-Type", "application/json; utf-8");
        con.setRequestProperty("Authorization", "Bearer " + userDto.getToken());
        con.setDoOutput(true);

        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8))) {
            StringBuilder response = new StringBuilder();
            String responseLine;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }
            PageDto page = new Gson().fromJson(response.toString(), PageDto.class);
            sendMessage.setText(MessageTemplates.SCHEDULED_TASKS.getMessage());
            sendMessage.setReplyMarkup(inlineKeyboard.getTasksButtonKeyboard(page));
            return sendMessage;
        }
    }

    public UserDto getUserDto() {
        return userDto;
    }
}
