package ru.coderiders.tgbot.config;

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.telegram.telegrambots.meta.api.methods.updates.SetWebhook;
import ru.coderiders.tgbot.bot.Bot;
import ru.coderiders.tgbot.handler.CallbackQueryHandler;
import ru.coderiders.tgbot.handler.MessageHandler;

@Configuration
@AllArgsConstructor
public class SpringConfig {
    private final TelegramConfig telegramConfig;

    @Bean
    public Bot springBot(MessageHandler messageHandler,
                                         CallbackQueryHandler callbackQueryHandler) {
        Bot bot = new Bot(messageHandler, callbackQueryHandler);

        bot.setBotUsername(telegramConfig.getBotName());
        bot.setBotToken(telegramConfig.getBotToken());

        return bot;
    }
}

