package ru.coderiders.tgbot.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class PageDto {
    List<TaskDto> content;
    int totalElements;
    int numberOfElements;
    int totalPages;
    int number;
}
