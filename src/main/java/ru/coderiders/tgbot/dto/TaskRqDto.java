package ru.coderiders.tgbot.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class TaskRqDto {
    private String type;
    private String expiresDateTime;
    private String repeatType;
    private boolean completed;
    private Long animalId;
    private String note;
}
