package ru.coderiders.tgbot.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class TaskDto {

    private Long id;
    private String type;
    private String expiresDateTime;
    private boolean completed;
    private String repeatType;
    private String note;
    private Long animalId;
    private String animalName;
    private String animalKind;
}
