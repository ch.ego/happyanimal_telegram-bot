package ru.coderiders.tgbot.bot;

import com.google.gson.Gson;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.coderiders.tgbot.constants.ButtonNames;
import ru.coderiders.tgbot.dto.PageDto;
import ru.coderiders.tgbot.dto.TaskDto;
import ru.coderiders.tgbot.dto.UserDto;
import ru.coderiders.tgbot.handler.CallbackQueryHandler;
import ru.coderiders.tgbot.handler.MessageHandler;
import ru.coderiders.tgbot.utils.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

@Slf4j
@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Bot extends TelegramLongPollingBot {
    String botUsername;
    String botToken;

    MessageHandler messageHandler;
    CallbackQueryHandler callbackQueryHandler;

    InlineKeyboard inlineKeyboard;
    String chatId;

    public Bot(MessageHandler messageHandler, CallbackQueryHandler callbackQueryHandler) {
        this.messageHandler = messageHandler;
        this.callbackQueryHandler = callbackQueryHandler;
        this.inlineKeyboard = new InlineKeyboard();
    }

    @Override
    public String getBotUsername() {
        return botUsername;
    }

    @Override
    public String getBotToken() {
        return botToken;
    }

    private BotApiMethod<?> handleUpdate(Update update) throws IOException, TelegramApiException, URISyntaxException, InterruptedException, ExecutionException {
        if (update.hasCallbackQuery()) {
            if (chatId == null) chatId = update.getCallbackQuery().getMessage().getChatId().toString();
            CallbackQuery callbackQuery = update.getCallbackQuery();
            return callbackQueryHandler.processCallbackQuery(callbackQuery);
        } else {
            Message message = update.getMessage();
            if (chatId == null) chatId = update.getMessage().getChatId().toString();
            if (message != null) {
                return messageHandler.answerMessage(update.getMessage());
            }
        }
        return null;
    }

    @Override
    public void onUpdateReceived(Update update) {
        try {
            execute(handleUpdate(update));
        } catch (IOException | TelegramApiException | URISyntaxException | InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpdatesReceived(List<Update> updates) {
        super.onUpdatesReceived(updates);
    }

    @Scheduled(cron = "0 */2 * * * *")
    public void sendNotificationsEvery2Min() throws IOException {
        UserDto userDto = messageHandler.getUserDto();
        int total;
        if (userDto != null) {
            String url = "http://localhost:8080/api/tasks?userId=" + userDto.getId() + "&sort=id";
            HttpURLConnection con = getUrlConnection(url, userDto);
            String response = readResponse(con);
            if (!response.isBlank()) {
                PageDto page = new Gson().fromJson(response, PageDto.class);
                total = page.getTotalElements();

                String url2 = "http://localhost:8080/api/tasks?&size=" + total + "&userId=" + userDto.getId() + "&sort=id";
                HttpURLConnection con2 = getUrlConnection(url2, userDto);
                String response2 = readResponse(con2);
                PageDto page2 = new Gson().fromJson(response2, PageDto.class);
                List<TaskDto> tasks = page2.getContent()
                        .stream()
                        .filter(taskDto -> LocalDateTime.parse(taskDto.getExpiresDateTime(), DateTimeFormatter.ISO_DATE_TIME)
                                .isAfter(LocalDateTime.now()))
                        .collect(Collectors.toList());
                tasks.forEach(taskDto -> {
                    LocalDateTime currentTime = LocalDateTime.now();
                    LocalDateTime expiresDT = LocalDateTime.parse(taskDto.getExpiresDateTime(), DateTimeFormatter.ISO_DATE_TIME);
                    if (currentTime.plusMinutes(10).plusSeconds(2).isAfter(expiresDT)
                            && chatId != null
                            && !taskDto.isCompleted()) {
                        try {
                            execute(SendMessage.builder()
                                    .chatId(chatId)
                                    .text(StringUtils.formatTaskMessage(taskDto))
                                    .replyMarkup(inlineKeyboard.getOneButtonKeyboard(ButtonNames.GET_ONE_TASK.getName(),
                                            "id: " + taskDto.getId().toString()))
                                    .build());
                        } catch (TelegramApiException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }
    }

    private String readResponse(HttpURLConnection con) throws IOException {
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8))) {
            StringBuilder response = new StringBuilder();
            String responseLine;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }
            return response.toString();
        }
    }


    private HttpURLConnection getUrlConnection(String address, UserDto userDto) throws IOException {
        URL url = new URL(address);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("Content-Type", "application/json; utf-8");
        con.setRequestProperty("Authorization", "Bearer " + userDto.getToken());
        con.setDoOutput(true);
        return con;
    }
}
