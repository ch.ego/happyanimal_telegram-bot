package ru.coderiders.tgbot.bot;

import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import ru.coderiders.tgbot.constants.ButtonNames;

import java.util.ArrayList;
import java.util.List;

@Component
public class Keyboard {
    public ReplyKeyboardMarkup getMainMenuKeyboard() {
        return getSingleButtonKeyboard(ButtonNames.GET_TASKS.getName());
    }

    private ReplyKeyboardMarkup getSingleButtonKeyboard(String buttonName) {
        KeyboardRow row1 = new KeyboardRow();
        row1.add(new KeyboardButton(buttonName));

        List<KeyboardRow> keyboard = new ArrayList<>();
        keyboard.add(row1);

        final ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        replyKeyboardMarkup.setKeyboard(keyboard);
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(false);

        return replyKeyboardMarkup;
    }
}
