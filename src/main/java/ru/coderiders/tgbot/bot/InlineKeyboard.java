package ru.coderiders.tgbot.bot;

import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import ru.coderiders.tgbot.constants.ButtonNames;
import ru.coderiders.tgbot.dto.PageDto;
import ru.coderiders.tgbot.dto.TaskDto;
import ru.coderiders.tgbot.utils.StringUtils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class InlineKeyboard {
    public InlineKeyboardMarkup getAuthorizationKeyboard() {
        List<List<InlineKeyboardButton>> allButtons = Collections.singletonList(
                new ArrayList<>(Collections.singletonList(
                        getSingleInlineButton(ButtonNames.AUTHORIZE.getName(), ButtonNames.AUTHORIZE.getName()))));
        InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
        markupInline.setKeyboard(allButtons);
        return markupInline;
    }

    public InlineKeyboardMarkup getTasksButtonKeyboard(PageDto taskDtoPage) {
        List<TaskDto> tasks = taskDtoPage.getContent()
                .stream()
                .filter(taskDto -> !taskDto.isCompleted())
                .dropWhile(taskDto -> taskDtoPage.getContent().indexOf(taskDto) < taskDtoPage.getNumber() * 8)
                .limit(8)
                .collect(Collectors.toList());
        List<List<InlineKeyboardButton>> allButtons = new ArrayList<>();
        tasks.forEach(taskDto -> {
            LocalDateTime localDateTime = LocalDateTime.parse(taskDto.getExpiresDateTime(), DateTimeFormatter.ISO_DATE_TIME);
            allButtons.add(Collections.singletonList(
                    getSingleInlineButton(taskDto.getType() + "[" +
                                    taskDto.getAnimalName() + "] - " +
                                    StringUtils.formatDateTime(localDateTime),
                            "id: " + taskDto.getId().toString())));
        });
        /*if (taskDtoPage.getTotalPages() > taskDtoPage.getNumber() - 1) {
            int nextPage = taskDtoPage.getNumber() + 1;
            allButtons.add(Collections.singletonList(
                    getSingleInlineButton(ButtonNames.NEXT.getName(), "next: " + nextPage)));
        }*/
        InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
        markupInline.setKeyboard(allButtons);
        return markupInline;
    }

    public InlineKeyboardMarkup getOneButtonKeyboard(String text, String callbackData) {
        InlineKeyboardButton button = getSingleInlineButton(text, callbackData);
        List<List<InlineKeyboardButton>> allButtons = Collections.singletonList(
                new ArrayList<>(Collections.singletonList(button)));
        InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
        markupInline.setKeyboard(allButtons);
        return markupInline;
    }

    public InlineKeyboardButton getSingleInlineButton(String text, String callbackData) {
        return InlineKeyboardButton.builder()
                .text(text)
                .callbackData(callbackData)
                .build();
    }

    public InlineKeyboardMarkup getDoneKeyboard(String taskId) {
        List<List<InlineKeyboardButton>> allButtons = Collections.singletonList(
                new ArrayList<>(Collections.singletonList(
                        getSingleInlineButton(ButtonNames.TASK_DONE.getName(), "doneId: " + taskId))));
        InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
        markupInline.setKeyboard(allButtons);
        return markupInline;
    }
}
