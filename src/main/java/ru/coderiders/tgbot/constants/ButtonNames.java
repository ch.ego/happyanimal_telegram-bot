package ru.coderiders.tgbot.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ButtonNames {
    GET_TASKS("Получить задачи"),
    AUTHORIZE("Авторизоваться"),
    GET_ONE_TASK("Посмотреть задачу"),
    NEXT("➡"),
    TASK_DONE("Я сделал(-а) это");
    private final String name;


}
