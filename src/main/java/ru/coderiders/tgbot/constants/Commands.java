package ru.coderiders.tgbot.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Objects;

@Getter
@AllArgsConstructor
public enum Commands {
    START("/start");
    private final String name;

    public static Commands getCommandBy(String name){
        for (Commands value : values()) {
            if (Objects.equals(value.name, name)){
                return value;
            }
        }
        return null;
    }
}