package ru.coderiders.tgbot.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MessageTemplates {
    GREETING("Приветствуем, это бот для смотрителей зоопарка. " +
            "Здесь ты можешь увидеть свои предстоящие задачи"),
    PLEASE_ENTER_CREDENTIALS("Пожалуйста, введите данные"),
    AUTHORIZATION_SUCCESSFUL("Авторизация прошла успешно"),
    SCHEDULED_TASKS("Предстоящие задачи:"),
    EXCEPTION_ILLEGAL("Непредвиденная ошибка, попробуйте позже"),
    TASK_COMPLETED("Задачка успешно сделана. Ты молодец возьми печеньку \uD83C\uDF6A");
    private final String message;
}
