package ru.coderiders.tgbot.utils;

import ru.coderiders.tgbot.dto.TaskDto;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class StringUtils {
    public static String formatDateTime(LocalDateTime dateTime) {
        return formatNumber(dateTime.getDayOfMonth()) + "." +
                formatNumber(dateTime.getMonthValue()) + " " +
                formatNumber(dateTime.getHour()) + ":" +
                formatNumber(dateTime.getMinute());
    }

    public static String formatNumber(int number) {
        if (number < 10) {
            return "0" + number;
        } else {
            return String.valueOf(number);
        }
    }

    public static String formatTask(TaskDto taskDto) {
        return "Задача #" + taskDto.getId() + "\n" +
                "Тип задачи: " + taskDto.getType() + "\n" +
                "Животное: " + taskDto.getAnimalName() + ", " + taskDto.getAnimalKind() + "\n" +
                "Дата и время истечения: " + formatDateTime(LocalDateTime.parse(taskDto.getExpiresDateTime(),
                DateTimeFormatter.ISO_DATE_TIME)) + "\n" +
                "Тип повторения: " + taskDto.getRepeatType() + "\n" +
                "Состояние: " + (taskDto.isCompleted() ? "завершена" : "не завершена");
    }

    public static String formatTaskMessage(TaskDto taskDto) {
        return "Задача #" + taskDto.getId() + "\n" +
                taskDto.getType() +
                " [" + taskDto.getAnimalName() + ", "
                + taskDto.getAnimalKind() + "] " + "\n"
                + formatDateTime(LocalDateTime.parse(taskDto.getExpiresDateTime(), DateTimeFormatter.ISO_DATE_TIME)) +
                "\nскоро завершится";
    }
}
